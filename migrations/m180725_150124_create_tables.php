<?php

use yii\db\Migration;

/**
 * Class m180725_150124_create_tables
 */
class m180725_150124_create_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('user', [
           'id' => $this->primaryKey(),
           'username' => $this->string(255)->notNull(),
           'status' => $this->integer()->notNull(),
           'balance' => $this->decimal(10, 5)->defaultValue(0)
        ]);

        $this->createTable('transaction', [
           'id' => $this->primaryKey(),
           'sender' => $this->integer()->notNull(),
           'recipient' => $this->integer()->notNull(),
           'type' => $this->integer()->notNull(),
           'amount' => $this->decimal(10, 5)->notNull(),
            'status' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx-transaction-sender',
            'transaction',
            'sender'
        );

        $this->createIndex(
            'idx-transaction-recipient',
            'transaction',
            'recipient'
        );

        $this->createIndex(
            'idx-transaction-type',
            'transaction',
            'type'
        );

        $this->addForeignKey(
            'fk-transaction-sender',
            'transaction',
            'sender',
            'user',
            'id',
            'NO ACTION',
            'NO ACTION'
        );

        $this->addForeignKey(
            'fk-transaction-recipient',
            'transaction',
            'recipient',
            'user',
            'id',
            'NO ACTION',
            'NO ACTION'
        );

        $this->insert('user',array(
            'username'=>'Николай',
            'status' => '1',
            'balance' => '1500'
        ));

        $this->insert('user',array(
            'username'=>'Фёдор',
            'status' => '1',
            'balance' => '5000'
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-transaction-sender', 'transaction');
        $this->dropForeignKey('fk-transaction-recipient', 'transaction');
        $this->dropTable('transaction');
        $this->dropTable('user');
    }

}
