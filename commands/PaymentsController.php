<?php

namespace app\commands;

use app\models\PaymentsUser;
use app\models\Transaction;
use Yii;
use yii\console\Controller;

class PaymentsController extends Controller
{

    public function actionReceive($sender, $recipient, $pay)
    {
        $pay = abs((round($pay, 5)));

        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        $senderModel = PaymentsUser::findBySql('SELECT * from user WHERE id='.$sender.' FOR UPDATE')->one();
        $recipientModel = PaymentsUser::findBySql('SELECT * from user WHERE id='.$recipient.' FOR UPDATE')->one();

        if($senderModel != null && $recipientModel != null && $senderModel->balance >= $pay) {

            $payment = new Transaction();
            $payment->sender = $sender;
            $payment->recipient = $recipient;
            $payment->type = 1;
            $payment->status = 1;
            $payment->amount = -1 * $pay;

            $senderModel->balance = (double)$senderModel->balance - $pay;
            $recipientModel->balance = round((double)$recipientModel->balance + $pay, 5);

            if(!$payment->save() || !$senderModel->save() || !$recipientModel->save()) {
                $transaction->rollBack();
                echo "Some problem occurred when save model\n";
                return 1;
            }

            $transaction->commit();
            echo "Successful\n";
            return 0;
        }

        $transaction->rollBack();
        echo "Some problem occurred\n";
        return 1;
    }
}